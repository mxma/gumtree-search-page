import { useEffect, useRef, useReducer, Fragment } from 'react';

const ACTIONS = ['View', 'Reply'];

const useFetch = (url) => {
    const cache = useRef({});

    const initialState = {
        error: null,
        data: [],
    };

    const [state, dispatch] = useReducer((state, action) => {
        switch (action.type) {
            case 'FETCHING':
                return { ...initialState, status: 'fetching' };
            case 'FETCHED':
                return {
                    ...initialState,
                    status: 'fetched',
                    data: action.payload,
                };
            case 'FETCH_ERROR':
                return {
                    ...initialState,
                    status: 'error',
                    error: action.payload,
                };
            default:
                return state;
        }
    }, initialState);

    useEffect(() => {
        let cancelRequest = false;
        if (!url) return;

        const fetchData = async () => {
            dispatch({ type: 'FETCHING' });
            if (cache.current[url]) {
                const data = cache.current[url];
                dispatch({ type: 'FETCHED', payload: data });
            } else {
                try {
                    const response = await fetch(url);
                    const data = await response.json();
                    cache.current[url] = data;
                    if (cancelRequest) return;
                    dispatch({ type: 'FETCHED', payload: data });
                } catch (error) {
                    if (cancelRequest) return;
                    dispatch({ type: 'FETCH_ERROR', payload: error.message });
                }
            }
        };

        fetchData();

        return function cleanup() {
            cancelRequest = true;
        };
    }, [url]);

    return state;
};

const formatAsCurrency = (int) => {
    // TODO
    const formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
        minimumFractionDigits: 0,
    });
    return formatter.format(int);
};

const handleClick = (action, title) => {
    console.log(`${action}: ${title}`);
};

const Listing = (props) => {
    // TODO
    // This should be the component which renders an individual listing to the page
    const { title, description, imgUrl, price, location } = props;
    return (
        <div className='listing'>
            <div className='listing__title'>{title}</div>
            <div>
                <span className='listing__price'>
                    {isNaN(price) ? price : formatAsCurrency(price)}
                </span>
                <span className='listing__location'>{location}</span>
            </div>
            <div className={imgUrl && 'listing__img'}>
                {imgUrl && <img src={imgUrl} alt={title} />}
            </div>
            <div className='listing__description'>{description}</div>
            <button
                className='listing__button'
                onClick={() => handleClick(ACTIONS[0], title)}
            >
                View
            </button>
            <button
                className='listing__button'
                onClick={() => handleClick(ACTIONS[1], title)}
            >
                Reply
            </button>
        </div>
    );
};

const Listings = (props) => {
    // TODO
    // This component should make a request to the api endpoint (props.dataEndpoint)
    // then render the result as set of listings as per the design mocks
    // check props passed in from parent for other values that you may need to use
    const { status, data, error } = useFetch(props.dataEndpoint);
    const length = (status === 'fetched' && data.length) || 0;
    console.log(data);
    return (
        <div>
            <div className='listings__header'>
                <div className='listings__header__results'>Search Results</div>
                {status === 'fetched' && (
                    <div>
                        <span className='listings__header__length'>
                            {length}
                        </span>{' '}
                        results for
                        <span className='listings__header__keyword'>
                            {' '}
                            Ferrari
                        </span>{' '}
                        in
                        <span className='listings__header__country'>
                            {' '}
                            Australia
                        </span>
                    </div>
                )}
            </div>
            <div className='listings__grid'>
                {status === 'error' && (
                    <div className='error'>API Error: {error}</div>
                )}
                {status === 'fetching' && (
                    <div className='loading'>Loading...</div>
                )}
                {status === 'fetched' &&
                    data.map((product) => {
                        const listingProps = {
                            title: product.title || '',
                            description: product.description || '',
                            imgUrl: product.imgUrl || '',
                            price: product.price || 0,
                            location: product.location || '',
                        };
                        return (
                            <Fragment key={listingProps.title}>
                                <Listing {...listingProps} />
                            </Fragment>
                        );
                    })}
            </div>
        </div>
    );
};

export default Listings;
